<?php
/*
 Plugin Name: VS Custom Upload Dir
 Description: Custom plugin to determine upload directory based on custom field 
 Version: 0.1
*/

add_action('admin_head-post.php', 'vs_upload_dir_javascript');
add_action('admin_head-post-new.php', 'vs_upload_dir_javascript');

add_filter('upload_dir', 'vs_upload_dir_custom');

// need this because sometimes wp_get_current user gets called out of place
require_once(ABSPATH . 'wp-includes/pluggable.php'); 

function vs_upload_dir_javascript() {
    global $post, $post_id;  
?>

<script type="text/javascript" >
jQuery(document).ready(function($) {

  var post_id = '<?php echo $post_id; ?>';
  var title = '<?php echo $post->post_title; ?>';    
  
  var filefields = ['file_default', 'file_hi_res', 'file_small'];
  var emptyArray = ["", 0, "0", null];
  var customfield;
    
  for (filefield in filefields) {
      jQuery('a[href*="'+filefield+'"]').addClass('custom-field-clickable');
  }         
  jQuery('a#set-post-thumbnail').addClass('custom-field-clickable');
  
  //alert('postid:' + post_id + ' title:' + title); 
         
  if(jQuery.inArray(post_id, emptyArray) !== -1 || title == '') {

        jQuery('.custom-field-clickable').addClass('post-not-saved').removeClass('custom-field-clickable').removeClass('thickbox').removeAttr('href');    
    }   
    
    jQuery('.post-not-saved').click(function () {
       alert('Please name and save the post before uploading, as the post ID and title are used to generate the file name and storage directory');    
    });     

    jQuery('a#set-post-thumbnail').not('.post-not-saved').click(function () {
          
	    var data = {
		    action: 'custom_field_click',
		    customfield: 'featured-image'
	    };
	   
	    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
	    jQuery.post(ajaxurl, data, function(response) {
		    //alert('Got this from the server: ' + response);
	    });	    
    });
    
     jQuery('a[onclick*="cft_clicked_id"]').not('.post-not-saved').click(function () {
                    
	      var data = {
		    action: 'custom_field_click',
		    customfield: 'media-file'
	      };

	      jQuery.post(ajaxurl, data, function(response) {
		    //alert('Got this from the server: ' + response);
	      });	    
    }); 
    
    jQuery('#media-buttons a').click(function () {
                    
	      var data = {
		    action: 'custom_field_click',
		    customfield: 'normal'
	      };

	      jQuery.post(ajaxurl, data, function(response) {
		    //alert('Got this from the server: ' + response);
	      });	    
    }); 
    
     
});
</script>
<?php
}

add_action('wp_ajax_custom_field_click', 'vs_upload_dir_ajaxcallback');

function vs_upload_dir_ajaxcallback() {
	global $wpdb; // this is how you get access to the database  
	
	// Idealy this should be done using Session variables, 
	// but there is some crazy thing going on where it always gets set to 2
	$current_user = wp_get_current_user();
	if(in_array($_POST['customfield'], array('featured-image','media-file','normal'))) {
	   update_option( 'vs-uploadfield-'. $current_user->ID, $_POST['customfield']);
	  //$_SESSION['customfield'] = 'featured-image';	  
	}  
	
	//echo $_POST['customfield'];  
	
	die(); // this is required to return a proper result
}

/*
$path
	[path] => /home/user/server.com/blog/wp-content/uploads/2008/11 
	[url] => http://blog.server.com/wp-content/uploads/2008/11 
	[subdir] => /2008/11 
	[basedir] => /home/user/server.com/blog/wp-content/uploads 
	[baseurl] => http://blog.server.com/wp-content/uploads 
	[error] => 	
	//since WP 2.7, this filter is called for each attachment displayed in the gallery-view.
*/
function vs_upload_dir_custom($path) {
    
    $current_user = wp_get_current_user();
    //$customfield = get_option( 'vs-uploadfield-'. $current_user->ID);
    global $post, $post_id;
    
    //print var_export($_POST);
    //echo $post_id;
	
    $customfield = get_option('vs-uploadfield-'. $current_user->ID);
    
    $post_id = $_POST['post_id'];
    $post = get_post($post_id);
        
    if(!empty($path['error'])){ return $path['error']; 	}	    
	if($_POST['Upload'] == '' && $_POST['html-upload'] == '') { return $path; }
    
    if ($customfield == 'featured-image') {          
          $customdir = 	'featured-images';
    }
    else if($customfield == 'media-file') {    
          $customdir = 	'media/' . sanitize_title($post->post_title) . '-' . $post->ID;
    }          
    else $customdir = 'page-images/' . date("Y");        

    $customdir = ($customdir && $customdir[0] !== '/') ? '/'.$customdir : $customdir;
	$customdir = untrailingslashit($customdir);	
	$path['path'] 	 = str_replace($path['subdir'], '', $path['path']); //remove default subdir (year/month)
	$path['url']	 = str_replace($path['subdir'], '', $path['url']);		
	$path['subdir']  = $customdir;
	$path['path'] 	.= $customdir; 
	$path['url'] 	.= $customdir;	
	
	//echo $path['path'];
	//echo $path['url'];
	
	if(!wp_mkdir_p( $path['path'])){		
		return array('error' => sprintf(__('Unable to create directory %s. Is its parent directory writable by the server?'), $path['path']));
	}
	
	//unset($_SESSION['customfield']);
	//delete_option('vs-uploadfield-'. $current_user->ID . '-featured-image');		
	return $path;
}


add_filter('sanitize_file_name', 'vs_custom_upload_rename', 10);

function vs_custom_upload_rename($filename) {
        
     //print var_export($filename);
     $current_user = wp_get_current_user();
     
     $info = pathinfo($filename);
     $ext  = empty($info['extension']) ? '' : '.' . $info['extension'];

     $customfield = get_option('vs-uploadfield-'. $current_user->ID);
    
    $post_id = $_POST['post_id'];
    $post = get_post($post_id);
    
    if ($customfield == 'featured-image') 
    {          
          return sanitize_title($post->post_title) . '-' . $post_id . $ext;
    }
    else {   
       return $filename;
    }   
         
} 




	